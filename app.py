from flask import Flask, jsonify
from ipfraud import database


app = Flask(__name__)
app.url_map.strict_slashes = False

INDEX = f"""
    <html>
        <body>
            <h1>IP FRAUD CHECK API!</h1>
            <h3>Check Fraud Score of Your Ip Address</h3>
            <h6>©Copyright by: Nitin</h6>
            <h6>Note: My Codes Fetch Info From scamalytics and i dont owe that</h6>
            <h6>Source code: <a href="https://gitlab.com/nitin1818/fraudchk">gitlab.com/Nitin1818/fraudchk</a></h6>
        </body>
    </html>
    """

@app.route("/")
def home():
    return INDEX

@app.route("/<query>")
def data_(query):
    return jsonify(database(query))


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080, debug=True)
